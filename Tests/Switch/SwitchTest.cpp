#include <Io.hpp>
#include <StringView.hpp>

int main()
{
    using namespace hsd::format_literals;
    using namespace hsd::string_view_literals;
    
    hsd::switch_cases("Test value"_sv, 
        hsd::equal_case{"test",       []{hsd::println("1"_fmt);}},
        hsd::equal_case{"value",      []{hsd::println("2"_fmt);}},
        hsd::equal_case{"Test",       []{hsd::println("3"_fmt);}},
        hsd::equal_case{"Value",      []{hsd::println("4"_fmt);}},
        hsd::equal_case{"test value", []{hsd::println("5"_fmt);}},
        hsd::equal_case{"test Value", []{hsd::println("6"_fmt);}},
        hsd::equal_case{
            "Test value", []() -> hsd::option_err<hsd::runtime_error>
            {
                hsd::println("7"_fmt);
                return {};
                //return hsd::runtime_error{"Got the 7th value"};
            }
        },
        hsd::equal_case{"Test Value", []{hsd::println("8"_fmt);}},
        hsd::default_case{[]{hsd::print("Default"_fmt);}}
    );

    hsd::switch_cases(
        -234,
        hsd::less_case   {120, []{hsd::println("Less than 120"_fmt   );}},
        hsd::equal_case  {120, []{hsd::println("Equal than 120"_fmt  );}},
        hsd::greater_case{120, []{hsd::println("Greater than 120"_fmt);}},
        hsd::default_case{
            []() -> hsd::option_err<hsd::runtime_error>
            {
                return hsd::runtime_error{"It's impossible to get here"};
            }
        }
    );

    hsd::switch_cases("Test value"_sv, 
        hsd::less_case   {"Tf", []{hsd::println("Less than Tf\n"_fmt   );}},
        hsd::equal_case  {"Tf", []{hsd::println("Equal than Tf\n"_fmt  );}},
        hsd::greater_case{"Tf", []{hsd::println("Greater than Tf\n"_fmt);}},
        hsd::default_case{[]{hsd::println_err("HOW"_fmt);}}
    );
}