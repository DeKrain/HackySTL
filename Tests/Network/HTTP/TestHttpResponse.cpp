#include <Extra/HttpResponse.hpp>
#include <Io.hpp>

using namespace hsd::string_view_literals;

constexpr auto resp = 
    "HTTP/1.1 200 \r\n"
    "Content-Type: text/html\r\n"
    "Content-Length: 13\r\n"
    "\r\nHello world\r\n"_sv;

int main()
{
    using namespace hsd::format_literals;
    using namespace hsd::string_literals;

    hsd::http::response http_response;
    hsd::string protocol;

    auto len = 
        http_response.
        set_version(hsd::http::protocol_version::ver_1_1).
        parse(resp).
        unwrap();

    hsd::println("{}"_fmt, len);

    switch (http_response.get_version())
    {
        case hsd::http::protocol_version::ver_0_9:
        {
            protocol = "HTTP/0.9"_s;
            break;
        }
        case hsd::http::protocol_version::ver_1_0:
        {
            protocol = "HTTP/1.0"_s;
            break;
        }
        case hsd::http::protocol_version::ver_1_1:
        {
            protocol = "HTTP/1.1"_s;
            break;
        }
        case hsd::http::protocol_version::ver_2_0:
        {
            protocol = "HTTP/2.0"_s;
            break;
        }
        case hsd::http::protocol_version::ver_3_0:
        {
            protocol = "HTTP/3.0"_s;
            break;
        }
    }

    hsd::println(
        "{} {} {}"_fmt, protocol, 
        static_cast<hsd::u16>(http_response.get_status()),
        http_response.get_message()
    );

    for (const auto& [field, value] : http_response.get_header())
    {
        hsd::println("{}: {}"_fmt, field, value);
    }
}