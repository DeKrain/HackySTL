#include <Extra/HttpRequest.hpp>

#include <Network.hpp>
#include <Io.hpp>

using namespace hsd::format_literals;
using namespace hsd::string_view_literals;

/*constexpr auto req = 
    "GET / HTTP/1.1\r\n"
    "Host: 127.0.0.1:54000\r\n"
    "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:103.0) "
    "Gecko/20100101 Firefox/103.0\r\n"
    "Accept: text/html,application/xhtml+xml,application/"
    "xml;q=0.9,image/avif,image/webp,*;q=0.8\r\n"
    "Accept-Language: en-US,en;q=0.5\r\n"
    "Accept-Encoding: gzip, deflate, br\r\n"
    "DNT: 1\r\n"
    "Connection: keep-alive\r\n"
    "Upgrade-Insecure-Requests: 1\r\n"
    "Sec-Fetch-Dest: document\r\n"
    "Sec-Fetch-Mode: navigate\r\n"
    "Sec-Fetch-Site: none\r\n"
    "Sec-Fetch-User: ?1\r\n"
    "Sec-GPC: 1\r\n"
    "\r\n"_sv;
*/

void handle_req(const char* req);

int main()
{
    hsd::tcp_server_v4 server = "0.0.0.0:54000";

    while (true)
    {
        server.poll();

        for (auto& socket : server)
        {
            char buf[65536]{};
            auto sz = socket.receive(buf, 65536);
            
            if (sz != 0)
            {
                handle_req(buf);
                return 0;
            }
        }
    }

    return 0;
}

void handle_req(const char* req)
{
    hsd::http::request http_request;

    auto len = 
        http_request.
        set_version(hsd::http::protocol_version::ver_1_1).
        parse(req).
        unwrap();

    hsd::println("{}"_fmt, len);

    using namespace hsd::string_literals;
    hsd::string method, protocol;

    switch (http_request.get_method())
    {
        case hsd::http::request_method::get:
        {
            method = "GET"_s;
            break;
        }
        case hsd::http::request_method::head:
        {
            method = "HEAD"_s;
            break;
        }
        case hsd::http::request_method::post:
        {
            method = "POST"_s;
            break;
        }
        case hsd::http::request_method::put:
        {
            method = "PUT"_s;
            break;
        }
        case hsd::http::request_method::del:
        {
            method = "DELETE"_s;
            break;
        }
        case hsd::http::request_method::connect:
        {
            method = "CONNECT"_s;
            break;
        }
        case hsd::http::request_method::options:
        {
            method = "OPTIONS"_s;
            break;
        }
        case hsd::http::request_method::trace:
        {
            method = "TRACE"_s;
            break;
        }
        case hsd::http::request_method::patch:
        {
            method = "PATCH"_s;
            break;
        }
    }

    switch (http_request.get_version())
    {
        case hsd::http::protocol_version::ver_0_9:
        {
            protocol = "HTTP/0.9"_s;
            break;
        }
        case hsd::http::protocol_version::ver_1_0:
        {
            protocol = "HTTP/1.0"_s;
            break;
        }
        case hsd::http::protocol_version::ver_1_1:
        {
            protocol = "HTTP/1.1"_s;
            break;
        }
        case hsd::http::protocol_version::ver_2_0:
        {
            protocol = "HTTP/2.0"_s;
            break;
        }
        case hsd::http::protocol_version::ver_3_0:
        {
            protocol = "HTTP/3.0"_s;
            break;
        }
    }

    hsd::println("{} {} {}"_fmt, method, http_request.get_path(), protocol);

    for (const auto& [field, value] : http_request.get_header())
    {
        hsd::println("{}: {}"_fmt, field, value);
    }

    hsd::println("data:\n{}"_fmt, req + len);
}