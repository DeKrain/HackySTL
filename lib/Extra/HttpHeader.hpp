#pragma once

#include "../UnorderedMap.hpp"
#include "HttpBase.hpp"

namespace hsd::http
{
    class header
    {
    private:
        unordered_map<string, string> _fields{};
        static inline string _buffer{};

        class header_error
        {
        private:
            string _err;

        public:
            inline header_error(const string_view& msg, usize line)
                : _err{msg + (" " + to_string(line))}
            {}

            const char* pretty_error() const
            {
                return _err.c_str();
            }
        };

    public:
        inline auto get_field(const string_view& key) const
        {
            // Because HTTP header fields are case-insensitive for some reason
            _buffer = key;
            cstring::lower(_buffer.data());

            return _fields.at(_buffer);
        }

        inline header& set_field(const string_view& key, const string_view& value)
        {
            // Because HTTP header fields are case-insensitive for some reason
            _buffer = key;
            cstring::lower(_buffer.data());

            _fields[_buffer] = value;
            return *this;
        }

        inline auto& clear()
        {
            _fields.clear();
            return *this;
        }

        inline auto begin() const
        {
            return _fields.begin();
        }

        inline auto end() const
        {
            return _fields.end();
        }

        inline result<usize, header_error> parse(const string_view& data)
        {
            using namespace string_view_literals;
            usize _line_start = 0, _num_lines = 1;

            while (data[_line_start] != '\r' && data[_line_start] != '\n')
            {
                auto _colum_pos = data.find(':', _line_start);
                auto _value_pos = _colum_pos + 1;

                if (_colum_pos == data.npos)
                {
                    return header_error{
                        "Could not find filed separator at line"_sv, _num_lines
                    };
                }

                for (; cstring::iswhitespace(data[_value_pos]); ++_value_pos)
                    ;

                auto _end_pos = data.find('\n', _value_pos);

                if (_end_pos == data.npos)
                {
                    return header_error{
                        "Missing newline terminator at line"_sv, _num_lines
                    };
                }

                if (data[_end_pos - 1] == '\r')
                {
                    --_end_pos;
                }
                else
                {
                    return header_error{
                        "HTTP requires headers to end the line with a CRLF"_sv, _num_lines
                    };
                }

                string _key{&data[_line_start], _colum_pos - _line_start};
                cstring::lower(_key.data());

                _fields.emplace(
                    move(_key), string {
                        &data[_value_pos], _end_pos - _value_pos
                    }
                );

                ++_num_lines;
                _line_start = _end_pos + 2;
            }

            return _line_start;
        }
    };
} // namespace hsd::http