#pragma once

#include "../Network.hpp"
#include "../SStream.hpp"

#include "HttpRequest.hpp"
#include "HttpResponse.hpp"

namespace hsd::http
{
    template <typename SockType>
    class socket
        : private sstream
    {
    private:
        SockType* _socket = nullptr;
        request _req;
        response _resp;

        static inline auto get_version_string(protocol_version ver)
        {
            using namespace string_view_literals;

            switch (ver)
            {
                case protocol_version::ver_0_9:
                {
                    return "HTTP/0.9"_sv;
                }
                case protocol_version::ver_1_0:
                {
                    return "HTTP/1.0"_sv;
                }
                case protocol_version::ver_1_1:
                {
                    return "HTTP/1.1"_sv;
                }
                case protocol_version::ver_2_0:
                {
                    return "HTTP/2.0"_sv;
                }
                case protocol_version::ver_3_0:
                {
                    return "HTTP/3.0"_sv;
                }
            }
        }

        static inline auto get_request_string(request_method method)
        {
            using namespace string_view_literals;

            switch (method)
            {
                case request_method::get:
                {
                    return "GET"_sv;
                }
                case request_method::head:
                {
                    return "HEAD"_sv;
                }
                case request_method::post:
                {
                    return "POST"_sv;
                }
                case request_method::put:
                {
                    return "PUT"_sv;
                }
                case request_method::del:
                {
                    return "DELETE"_sv;
                }
                case request_method::connect:
                {
                    return "CONNECT"_sv;
                }
                case request_method::options:
                {
                    return "OPTIONS"_sv;
                }
                case request_method::trace:
                {
                    return "TRACE"_sv;
                }
                case request_method::patch:
                {
                    return "PATCH"_sv;
                }
            }
        }

    public:
        inline auto& reserve_buffer(usize new_cap)
        {
            reserve(new_cap);
            return *this;
        }

        inline auto& set_version(protocol_version ver)
        {
            _req.set_version(ver);
            _resp.set_version(ver);
            return *this;
        }

        inline auto& set_current_sock(SockType& sock)
        {
            _socket = &sock;
            return *this;
        }

        inline auto& get_request()
        {
            return _req;
        }

        inline auto& get_response()
        {
            return _resp;
        }

        inline auto send_response(const string_view& data) -> option_err<runtime_error>
        {
            if (_socket == nullptr) [[unlikely]]
            {
                return runtime_error {
                    "No socket to refer to"
                };
            }

            this->write_data<"{} {} {}\r\n">(
                get_version_string(_resp.get_version()), 
                static_cast<u16>(_resp.get_status()),
                _resp.get_message()
            );

            for (const auto& [_field, _value] : _resp.get_header())
            {
                this->append_data<"{}: {}\r\n">(_field, _value);
            }

            this->append_data<"\r\n{}">(data);
            _socket->send(this->c_str(), this->size());
        
            return {};
        }

        inline auto send_request() -> option_err<runtime_error>
        {
            if (_socket == nullptr) [[unlikely]]
            {
                return runtime_error {
                    "No socket to refer to"
                };
            }

            this->write_data<"{} {} {}\r\n">(
                get_request_string(_req.get_method()),
                _req.get_path(),
                get_version_string(_req.get_version()) 
            );

            for (const auto& [_field, _value] : _req.get_header())
            {
                this->append_data<"{}: {}\r\n">(_field, _value);
            }

            this->append_data<"\r\n">();
            _socket->send(this->c_str(), this->size());
        
            return {};
        }

        inline auto receive_request() -> result<string_view, runtime_error>
        {
            if (_socket == nullptr) [[unlikely]]
            {
                return runtime_error {
                    "No socket to refer to"
                };
            }

            this->_size = _socket->receive(
                this->data(), this->capacity()
            );

            if (this->size() == 0)
            {
                // Not undefined, let the user
                using namespace hsd::string_view_literals;
                
                return ""_sv;
            }

            auto _res = _req.parse({this->data(), this->size()});
        
            if (!_res.is_ok())
            {
                return _res.unwrap_err();
            }

            auto _pos = _res.unwrap();

            return string_view {
                this->data() + _pos, this->size() - _pos
            };
        }
        
        inline auto receive_response() -> result<string_view, runtime_error>
        {
            if (_socket == nullptr) [[unlikely]]
            {
                return runtime_error {
                    "No socket to refer to"
                };
            }

            this->_size = _socket->receive(
                this->data(), this->capacity()
            );

            auto _res = _resp.parse({this->data(), this->size()});
        
            if (!_res.is_ok())
            {
                return _res.unwrap_err();
            }

            auto _pos = _res.unwrap();

            return string_view {
                this->data() + _pos, this->size() - _pos
            };
        }
    };
} // namespace hsd::http