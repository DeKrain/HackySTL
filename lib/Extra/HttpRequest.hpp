#pragma once

#include "HttpBase.hpp"
#include "HttpHeader.hpp"

namespace hsd::http
{
    class request
    {
    private:
        protocol_version _ver = protocol_version::ver_1_1;
        request_method _method{};
        string _path{};
        header _header{};

    public:
        inline const auto& get_header() const
        {
            return _header;
        }

        inline auto& clear()
        {
            _path.clear();
            _header.clear();
            return *this;
        }

        inline auto& set_header_field(
            const string_view& key, const string_view& value)
        {
            _header.set_field(key, value);
            return *this;
        }

        inline const auto& get_path() const
        {
            return _path;
        }

        inline auto& set_path(const string_view& path)
        {
            _path = path;
            return *this;
        }

        inline auto get_method() const
        {
            return _method;
        }

        inline auto& set_method(request_method method)
        {
            _method = method;
            return *this;
        }

        inline auto get_version() const
        {
            return _ver;
        }

        inline auto& set_version(protocol_version version)
        {
            _ver = version;
            return *this;
        }

        inline auto parse(const string_view& req)
            -> result<usize, runtime_error>
        {
            using namespace string_view_literals;
            usize _pos = req.find(' ');
            usize _begin_pos = 0;

            if (_pos == req.npos)
            {
                return runtime_error{
                    "Method separator not found"
                };
            }
            else
            {
                string_view _req_method = {
                    req.data(), _pos
                };

                check_request_method(_req_method, _method);
            }

            if (req[++_pos] != '/')
            {
                return runtime_error{
                    "Path for the requested resource is absent"
                };
            }
            else
            {
                _begin_pos = _pos;
                _pos = req.find(' ', _pos);
            }

            if (_pos == req.npos)
            {
                return runtime_error{
                    "Path separator not found"
                };
            }
            else
            {
                _path = string_view{
                    &req[_begin_pos], _pos - _begin_pos
                };

                _begin_pos = ++_pos;
                _pos = req.find('\n', _pos);
            }

            if (_pos == req.npos)
            {
                return runtime_error{
                    "Protocol terminator not found"
                };
            }
            else
            {
                if (req[_pos - 1] == '\r')
                {
                    --_pos;
                }
                else
                {
                    return runtime_error{
                        "HTTP requires to end the first line with a CRLF"
                    };
                }

                string_view _protocol = {
                    &req[_begin_pos], _pos - _begin_pos
                };

                check_protocol(_protocol, _ver);

                _pos += 2;
                return _pos + _header.parse({&req[_pos], req.size() - _pos}).unwrap();
            }
        } 
    };
} // namespace hsd::http