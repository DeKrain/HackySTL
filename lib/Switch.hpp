#pragma once

#include "Result.hpp"

namespace hsd
{
    namespace switch_detail
    {
        template <typename Func>
        concept InvocableVoid = (
            InvocableRet<void, Func> || requires(Func func) { 
                {func().unwrap()} -> IsSame<void>;
            }
        );
    } // namespace switch_detail

    namespace case_operators
    {
        template <typename T1, typename T2>
        concept EqualComparable = 
            requires(T1 a, T2 b) { {a == b} -> IsSame<bool>; };

        template <typename T1, typename T2>
        concept LessComparable = 
            requires(T1 a, T2 b) { {a < b} -> IsSame<bool>; };

        template <typename T1, typename T2>
        concept GreaterComparable = 
            requires(T1 a, T2 b) { {a > b} -> IsSame<bool>; };

        struct op_equal
        {
            template <typename T, typename U, typename Func>
            constexpr auto operator()(
                const T& left, const U& right, const Func& invoke_func)
            requires (EqualComparable<T, U> && switch_detail::InvocableVoid<Func>)
            {
                if constexpr (requires {invoke_func().unwrap();})
                {
                    using ErrType = decltype(invoke_func().unwrap_err());
                    using ResType = hsd::result<bool, ErrType>;

                    if (left == right)
                    {
                        auto _res = invoke_func();

                        if (!_res.is_ok())
                        {
                            return ResType{_res.unwrap_err()};
                        }
                        else
                        {
                            return ResType{true};
                        }
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
                else
                {
                    using ResType = hsd::result<bool, hsd::runtime_error>;

                    if (left == right)
                    {
                        invoke_func();
                        return ResType{true};
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
            }
        };

        struct op_less
        {
            template <typename T, typename U, typename Func>
            constexpr auto operator()(
                const T& left, const U& right, const Func& invoke_func)
            requires (LessComparable<T, U> && switch_detail::InvocableVoid<Func>)
            {
                if constexpr (requires {invoke_func().unwrap();})
                {
                    using ErrType = decltype(invoke_func().unwrap_err());
                    using ResType = hsd::result<bool, ErrType>;

                    if (left < right)
                    {
                        auto _res = invoke_func();

                        if (!_res.is_ok())
                        {
                            return ResType{_res.unwrap_err()};
                        }
                        else
                        {
                            return ResType{true};
                        }
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
                else
                {
                    using ResType = hsd::result<bool, hsd::runtime_error>;

                    if (left < right)
                    {
                        invoke_func();
                        return ResType{true};
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
            }
        };

        struct op_greater
        {
            template <typename T, typename U, typename Func>
            constexpr auto operator()(
                const T& left, const U& right, const Func& invoke_func)
            requires (GreaterComparable<T, U> && switch_detail::InvocableVoid<Func>)
            {
                if constexpr (requires {invoke_func().unwrap();})
                {
                    using ErrType = decltype(invoke_func().unwrap_err());
                    using ResType = hsd::result<bool, ErrType>;

                    if (left > right)
                    {
                        auto _res = invoke_func();

                        if (!_res.is_ok())
                        {
                            return ResType{_res.unwrap_err()};
                        }
                        else
                        {
                            return ResType{true};
                        }
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
                else
                {
                    using ResType = hsd::result<bool, hsd::runtime_error>;

                    if (left > right)
                    {
                        invoke_func();
                        return ResType{true};
                    }
                    else
                    {
                        return ResType{false};
                    }
                }
            }
        };
    } // namespace case_operators    

    template <typename CaseOp, typename T, typename Func>
    struct case_pair
    {
    private:
        T _value;
        Func _invoke_func;

    public:
        constexpr case_pair(T value, Func invoke_func)
            : _value{value}, _invoke_func{invoke_func}
        {}

        constexpr case_pair(const case_pair&) = default;
        constexpr case_pair& operator=(const case_pair&) = delete;

        template <typename U>
        requires (switch_detail::InvocableVoid<Func>)
        constexpr auto operator()(const U& compare_value) const
        {
            CaseOp _op_func;
            return _op_func(compare_value, _value, _invoke_func);
        }
    };

    template <typename Func>
    struct default_case
    {
    private:
        Func _invoke_func;

    public:
        constexpr default_case(Func invoke_func)
            : _invoke_func{invoke_func}
        {}

        constexpr default_case(const default_case&) = default;
        constexpr default_case& operator=(const default_case&) = delete;

        constexpr auto operator()() const
        requires (switch_detail::InvocableVoid<Func>)
        {
            using ResType = option_err<runtime_error>;

            if constexpr (requires {_invoke_func().unwrap();})
            {
                return _invoke_func();
            }
            else
            {
                _invoke_func();
                return ResType{};
            }
        }
    };

    template <typename T, typename Func> 
    static constexpr void switch_cases(const T&, default_case<Func> default_case)
    {
        default_case().unwrap();
    }

    template <typename Op, typename T, typename U, typename Func, typename... Pairs> 
    static constexpr void switch_cases(const T& compare_value, 
        case_pair<Op, U, Func> case_pair, Pairs... case_pairs)
    {
        if (case_pair(compare_value).unwrap()) return;
        switch_cases(compare_value, case_pairs...);
    }

    template <typename T, typename Func>
    class equal_case : 
        public case_pair<case_operators::op_equal, T, Func>
    {
    private:
        using _Base = case_pair<case_operators::op_equal, T, Func>;

    public:
        constexpr equal_case(const T& value, const Func& invoke_func)
            : _Base{value, invoke_func}
        {}

        constexpr equal_case(const equal_case& other) = default;
        constexpr equal_case& operator=(const equal_case&) = delete;
    };
    
    template <typename T, typename Func>
    class less_case : 
        public case_pair<case_operators::op_less, T, Func>
    {
    private:
        using _Base = case_pair<case_operators::op_less, T, Func>;

    public:
        constexpr less_case(const T& value, const Func& invoke_func)
            : _Base{value, invoke_func}
        {}

        constexpr less_case(const less_case& other) = default;
        constexpr less_case& operator=(const less_case&) = delete;
    };
    
    template <typename T, typename Func>
    class greater_case : 
        public case_pair<case_operators::op_greater, T, Func>
    {
    private:
        using _Base = case_pair<case_operators::op_greater, T, Func>;

    public:
        constexpr greater_case(const T& value, const Func& invoke_func)
            : _Base{value, invoke_func}
        {}

        constexpr greater_case(const greater_case& other) = default;
        constexpr greater_case& operator=(const greater_case&) = delete;
    };

    template <typename T, typename Func>
    equal_case(T, Func) -> equal_case<decay_t<T>, Func>;
    template <typename T, typename Func>
    less_case(T, Func) -> less_case<decay_t<T>, Func>;
    template <typename T, typename Func>
    greater_case(T, Func) -> greater_case<decay_t<T>, Func>;
} // namespace hsd